let scores, roundScore, activePlayer, gamePlaying;


init();



document.querySelector('.btn-roll').addEventListener('click', function() {
    if(gamePlaying) {
        // 1. Random number
        let dice = Math.floor(Math.random() * 6) + 1;

        //2. Display the result
        let diceDOM = document.querySelector('.dice');
        diceDOM.style.display = 'block';
        diceDOM.src = 'dice-' + dice + '.png';


        //3. Update the round score IF the rolled number was NOT a 1
        if (dice !== 1) {
            //Add score
            roundScore += dice;
            document.querySelector('#current-' + activePlayer).textContent = roundScore;
        } else {
            //Next player
            nextPlayer();
        }
    }    
});


document.querySelector('.btn-hold').addEventListener('click', ()=> {
    if(gamePlaying){
        //adds score from the round
    scores[activePlayer] += roundScore;
    
    //show changes on UI
    document.querySelector('#score-' + activePlayer).textContent = scores[activePlayer];
    
    if(scores[activePlayer] >= 20){
          document.querySelector('#name-' + activePlayer).textContent = 'Winner!';
            document.querySelector('.dice').style.display = 'none';
            document.querySelector('.player-' + activePlayer + '-panel').classList.add('winner');
            document.querySelector('.player-' + activePlayer + '-panel').classList.remove('active');
        gamePlaying = false;
    }
    else {
    nextPlayer();
    }
        
    }
    
});

function nextPlayer (){
     activePlayer === 0 ? activePlayer = 1 : activePlayer = 0;
        roundScore = 0;


        //if 1 was rolled delete the points from this 'round'
        document.getElementById('current-0').textContent = 0;
        document.getElementById('current-1').textContent = 0;

        //toggling the active class (gray background)    
        document.querySelector('.player-0-panel').classList.toggle('active');
        document.querySelector('.player-1-panel').classList.toggle('active');

//hiding the dice when player was switched
        document.querySelector('.dice').style.display = 'none';
}

document.querySelector('.btn-new').addEventListener('click', init)

function init(){
       scores = [0,0];
    activePlayer = 0;
    roundScore = 0;  
    gamePlaying = true;
    
    
document.querySelector('.dice').style.display = 'none';
document.getElementById('score-0').textContent = '0';
document.getElementById('score-1').textContent = '0';
document.getElementById('current-0').textContent = '0';
document.getElementById('current-1').textContent = '0';
    
     document.getElementById('name-0').textContent = 'Player 1';
     document.getElementById('name-1').textContent = 'Player 2';
    
         document.querySelector('.player-0-panel').classList.remove('winner');   document.querySelector('.player-1-panel').classList.remove('winner');
         document.querySelector('.player-0-panel').classList.remove('active');   
         document.querySelector('.player-1-panel').classList.remove('active');   
    document.querySelector('.player-0-panel').classList.add('active');
}
//document.querySelector('#current-' + activePlayer).innerHTML = dice;

console.log('Piggame - based on great js course made by Jonas Schmedtmann. Made some small changes (like building the project in ES6)')